﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreControllerScript : MonoBehaviour
{
    [SerializeField] private Text textScore;
    [SerializeField] private Text textHighScore;
    [SerializeField] private Text textEnemies;
    [SerializeField] private Text textRooms;

    // Start is called before the first frame update
    void Start()
    {
        textScore.text = "SCORE: " + PlayerPrefs.GetInt("score").ToString();

        if(PlayerPrefs.GetInt("score") == null)
        {
            PlayerPrefs.SetInt("highscore", PlayerPrefs.GetInt("score"));

        }else if (PlayerPrefs.GetInt("score") > PlayerPrefs.GetInt("highscore"))
        {
            PlayerPrefs.SetInt("highscore", PlayerPrefs.GetInt("score"));
        }

        textHighScore.text = "HIGHSCORE: "+PlayerPrefs.GetInt("highscore").ToString();
        textEnemies.text = "DEAD ENEMIES: " + PlayerPrefs.GetInt("deadEnemies").ToString();
        textRooms.text = "ROOMS COMPLETED: " + PlayerPrefs.GetInt("numRooms").ToString();

    }

   
}
