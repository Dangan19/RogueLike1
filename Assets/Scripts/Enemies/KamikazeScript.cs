﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KamikazeScript : DataEnemy
{
    [SerializeField] private float speed;   
    private Transform target;
    private float step;
    private ParticleSystem sysParticle;


    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.Find("Player").GetComponent<Transform>();
        sysParticle = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        float step = speed * Time.deltaTime;
        Vector3 direction = transform.position - target.position;

        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

        transform.position = Vector3.MoveTowards(transform.position, target.position, step);
        transform.rotation = Quaternion.AngleAxis(angle+90, Vector3.forward);

        Death();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player") {            
            GameManager.Instance.playerDamaged(5);
            sysParticle.Play();

            gameObject.GetComponent<PolygonCollider2D>().enabled = false;
            gameObject.GetComponent<SpriteRenderer>().enabled = false;

            SoundManagerScript.PlaySound("death");
            Destroy(gameObject, sysParticle.main.duration);


        }
    }

    
}
