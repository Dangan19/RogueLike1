﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackingSystem : MonoBehaviour
{
    [SerializeField] private float rotationSpeed = 3.0f;
    [SerializeField] private float bulletSpeed;
    [SerializeField] private Transform m_target;
    [SerializeField] private Rigidbody2D bullet;
    private Vector3 m_lastKnownPosition = Vector3.zero;
    private Quaternion m_lookAtRotation;
    private Vector3 angle;
    private Quaternion targetRotation;
    private Vector3 shootVector;

    private void Start()
    {
        InvokeRepeating("Shoot", 2.0f, 1f);
        m_target = GameObject.Find("Player").GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        //Traccking system
        m_lastKnownPosition = m_target.transform.position;

        shootVector = m_lastKnownPosition - transform.position;
        angle = Quaternion.Euler(0, 0, 180) * shootVector;

        targetRotation = Quaternion.LookRotation(forward: Vector3.forward, upwards: angle);

        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);

    
    }
    void Shoot() {

        Rigidbody2D instance = Instantiate(bullet, transform.position, targetRotation);
        shootVector.Normalize();
        instance.velocity = shootVector * bulletSpeed;

    }
}
