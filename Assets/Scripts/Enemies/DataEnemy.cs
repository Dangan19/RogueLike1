﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataEnemy : MonoBehaviour
{
    [SerializeField] public int enemyHealth;
    [SerializeField] private int enemyDamage;
    [SerializeField] private int scorePoints;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Death();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player") 
        {            
            GameManager.Instance.playerDamaged(enemyDamage);
            //Destroy(gameObject);
            enemyHealth = 0;
        }
    }

    protected void Death() {

        if (enemyHealth <= 0 ) {
            SoundManagerScript.PlaySound("death");
            Destroy(gameObject);
            GameManager.Instance.score += scorePoints;
            GameManager.Instance.deathEnemies++; 
            
        }
    
    }
}
