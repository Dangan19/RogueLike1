﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Upgrade 
{
    private string[] types = { "speed", "damage", "health" };
    private string type;
    private float amount;

    public Upgrade(int type, float amount)
    {
        this.amount = amount;
        this.type = types[type];
            
    }

    public float GetAmount()
    {
        return amount;
    }

    public string GetTypeUp()
    {
        return type;
    }

    
}
