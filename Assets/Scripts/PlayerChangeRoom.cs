﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerChangeRoom : MonoBehaviour
{
    [SerializeField] Transform startPosition;

    // Start is called before the first frame update
    void Start()
    {
        RoomManager.OnRoomChange += PlayerMove;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void PlayerMove()
    {
        gameObject.transform.position = startPosition.position;
    }
}
