﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxScript : MonoBehaviour
{
    public int durability;
    public List<GameObject> drops;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Destroyed();   
    }

    protected void Destroyed()
    {

        if (durability <= 0)
        {
            int index = Random.RandomRange(0, drops.Count);
            Debug.Log(index);
            GameManager.Instance.score += 5;

            Destroy(gameObject);
            Instantiate(drops[index], transform.position, Quaternion.identity);

        }

    }
}
