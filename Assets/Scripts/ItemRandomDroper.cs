﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemListDrops : ScriptableObject
{
    public List<ItemDataSC> items;
}
