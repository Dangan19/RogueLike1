﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ShopManager : MonoBehaviour
{
    public int[,] shopItems = new int[3,3];
    public Text scoreTxt;

    // Start is called before the first frame update
    void Start()
    {
        scoreTxt.text = "SCORE: " + GameManager.Instance.score.ToString();

        //ID's
        shopItems[0, 0] = 1;
        shopItems[0, 1] = 2;
        shopItems[0, 2] = 3;

        //Price
        shopItems[1, 0] = 10;
        shopItems[1, 1] = 10;
        shopItems[1, 2] = 10;
    }

    private void Update()
    {
        scoreTxt.text = "SCORE: " + GameManager.Instance.score.ToString();
    }

    public void Buy()
    {
        GameObject ButtonRef = GameObject.FindGameObjectWithTag("Event").GetComponent<EventSystem>().currentSelectedGameObject;

        if (GameManager.Instance.score >= shopItems[1, ButtonRef.GetComponent<ButtonInfo>().itemID]) {

            GameManager.Instance.score -= shopItems[1, ButtonRef.GetComponent<ButtonInfo>().itemID];
         

        }
    }
}
