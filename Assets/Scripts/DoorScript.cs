﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour
{
    // Start is called before the first frame update
    
    void Start()
    {
        RoomManager.OnRoomEnd += OpenDoor;
        RoomManager.OnRoomChange += CloseDoor;
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    void OpenDoor() => gameObject.SetActive(false);


    void CloseDoor() => gameObject.SetActive(true);
}
