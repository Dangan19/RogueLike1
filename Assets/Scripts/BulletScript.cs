﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    private int damage = 1;

    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    public void SetDamage(int d)
    {
        damage = d;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            collision.gameObject.GetComponent<DataEnemy>().enemyHealth -= damage;
            Destroy(gameObject);
            GameManager.Instance.score += 1;


        }else if (collision.gameObject.tag == "Box"){

            collision.gameObject.GetComponent<BoxScript>().durability -= damage;
            Destroy(gameObject);
        }
    }
}
