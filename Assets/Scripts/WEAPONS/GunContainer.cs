﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunContainer : MonoBehaviour
{

    public static readonly Gun cheap_Howitzer = new Gun(0.3f, 1, 0.010f, 2f, 2, 100000,  "Cheap Howitzer");
    public static readonly Gun smallShotgun = new Gun(1f, 5, 0.35f, 3.5f, 1, 200,  "Small Shotgun");
    public static readonly Gun dual_Howitzer = new Gun(0.3f, 2, 0.2f, 3f, 1, 500,  "Dual Howitzer");
    public static readonly Gun machine_gun = new Gun(0.2f, 3, 0.3f, 5f, 1, 300, "Machine Gun");
    public static readonly Gun bfg = new Gun(1f, 1, 0.01f, 2f, 10, 20, "BFG9000 ");


    public static List<Gun> guns;

    // Start is called before the first frame update
    void Start()
    {
        guns = new List<Gun>();
        guns.Add(cheap_Howitzer);  
        guns.Add(smallShotgun);
        guns.Add(dual_Howitzer);
        guns.Add(bfg);
        guns.Add(machine_gun);

    }

    public static Gun GetGun(int n)
    {
        return guns[n];
    }

    public static Gun GetRandomGun()
    {

        return guns[Random.Range(1, guns.Count)];
    }

}
