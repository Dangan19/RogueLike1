﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun
{
    private float fireRate;
    private float bullets;
    private float spread;
    private float speed;
    private int damage;
    private string name;
    private int ammo;
    private Sprite sprite;
   

    public Gun(float fireRate, float bullets, float spread, float speed, int damage, int ammo,  string name)
    {
        this.fireRate = fireRate;
        this.bullets = bullets;
        this.spread = spread;
        this.speed = speed;
        this.name = name;
        this.damage = damage;
        this.ammo = ammo;
        
    }

    public float GetFireRate()
    {
        return fireRate;
    }

    public float GetBullets()
    {
        return bullets;
    }

    public float GetSpread()
    {
        return spread;
    }

    public float GetSpeed()
    {
        return speed;
    }

    public int GetDamage()
    {
        return damage;
    }

    public string GetName()
    {
        return name;
    }

    public int GetAmmo()
    {
        return ammo;

    }

    public Sprite GetSprite()
    {
        return sprite;

    }

   
}
