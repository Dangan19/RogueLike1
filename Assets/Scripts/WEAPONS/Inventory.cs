﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Inventory : MonoBehaviour
{
    public AimScript aimScript;    
    public Gun gun1, gun2;
    private int activeGun = 0;
    public int ammoGun1, ammoGun2;
    public Text weaponText;


    // Start is called before the first frame update
    void Start()
    {
        gun1 = GunContainer.cheap_Howitzer;
        SetCurrentAmmo(gun1);

        
       
        

        aimScript.SetGun(GetCurrentGun(), GetCurrentAmmo());
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("f")) {
            SwapActive();
        
        }

        weaponText.text = GetCurrentGun().GetName();
    }

    public void SwapActive()
    {
        if (gun2 != null) {

            if (activeGun == 0)
            {
                activeGun = 1;
                ammoGun1 = GameManager.Instance.currentAmmo;

            }
            else
            {
                activeGun = 0;
                ammoGun2 = GameManager.Instance.currentAmmo;

            }


            aimScript.SetGun(GetCurrentGun(), GetCurrentAmmo());
        }
        
    }

    public void ResetInventory()
    {
        gun1 = null;
        gun2 = null;
        activeGun = 0;
    }

    public Gun GetCurrentGun()
    {
        if (activeGun == 0)
        {
            return gun1;
        }
        return gun2;
    }

    public int GetCurrentAmmo()
    {
        if (activeGun == 0)
        {
            return ammoGun1;

        }

        return ammoGun2;
    }

    public void SetCurrentAmmo(Gun g)
    {
        if (g == gun1)
        {
            ammoGun1 = g.GetAmmo();

        }
        else
        {
            ammoGun2 = g.GetAmmo();

        }

    }

    public void PickUpGun(Gun g)
    {
        gun2 = g;
        SetCurrentAmmo(gun2);

        aimScript.SetGun(GetCurrentGun(), GetCurrentAmmo());


    }

    
}
