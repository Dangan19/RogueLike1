﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Slider healthBar;

    private void Start()
    {
        healthBar = GetComponent<Slider>();

        healthBar.maxValue = GameManager.Instance.maxplayerHealth;
        healthBar.value = GameManager.Instance.playerHealth; ;
    }

    public void SetHealth(int hp)
    {
        healthBar.value = hp;
    }

    private void Update()
    {
        SetHealth(GameManager.Instance.playerHealth);
    }
}
