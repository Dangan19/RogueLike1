﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerScript : MonoBehaviour
{
    public static AudioClip fireSound;
    public static AudioClip deathSound;
    static AudioSource audioScr;

    // Start is called before the first frame update
    void Start()
    {
        fireSound = Resources.Load<AudioClip>("fireSound");
        deathSound = Resources.Load<AudioClip>("death");

        audioScr = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static void PlaySound(string clip)
    {
        switch (clip)
        {
            case "fire":
                audioScr.PlayOneShot(fireSound);
                break;

            case "death":
                audioScr.PlayOneShot(deathSound);
                break;

        }



    }
}
