﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject[] salas;
    public int currentSala = 0;
    public Camera mainCamera;

    // Start is called before the first frame update
    void Start()
    {
        RoomManager.OnRoomChange += CameraMove;

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void CameraMove()
    {
        currentSala++;
        mainCamera.transform.position = new Vector3(salas[currentSala].transform.position.x, salas[currentSala].transform.position.y, mainCamera.transform.position.z);

    }
}
