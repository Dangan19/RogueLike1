﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeValueChange : MonoBehaviour
{

    private AudioSource audioSrcMusic;
    private AudioSource audioSrcEfects;
    private float musicVolume = 1f;
    private float efectVolume = 1f;

    // Start is called before the first frame update
    void Start()
    {
        audioSrcMusic = GameObject.Find("").GetComponent<AudioSource>();
        audioSrcEfects = GameObject.Find("").GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        audioSrcMusic.volume = musicVolume;
        audioSrcMusic.volume = efectVolume;
    }

    public void SetVolume(float vol)
    {
        musicVolume = vol;
        efectVolume = vol;
    }
}
