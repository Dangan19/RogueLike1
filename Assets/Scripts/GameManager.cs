﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    public int maxplayerHealth;
    public int maxAmmo;
    public int currentAmmo;
    public int playerHealth;
    public int score;
    public int deathEnemies;
    public int numRooms;
    public HealthBar healthBar;
    public Text scoreText;
    public Text ammoText;    
    public bool invencible = false;

    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("Game Manager is NULL!");
            }

            return _instance;
        }


    }

    private void Awake()
    {
        _instance = this;
    }


    // Start is called before the first frame update
    void Start()
    {
        
        playerHealth = maxplayerHealth;
        healthBar.SetHealth(playerHealth);
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = score.ToString();
        ammoText.text = currentAmmo + " / " + maxAmmo;
    }

    public void playerDamaged(int damage)
    {
        

        playerHealth = playerHealth - damage;
        Debug.Log(playerHealth);
        healthBar.SetHealth(playerHealth);
        Debug.Log(playerHealth);

        if (playerHealth <= 0)
        {
            SaveScore();
            SoundManagerScript.PlaySound("death");
            //Application.LoadLevel(Application.loadedLevel);
            SceneManager.LoadScene("GameOver");
            Debug.Log("dead");
        }
        

    }

    public void SaveScore()
    {
         PlayerPrefs.SetInt("score", score);
        PlayerPrefs.SetInt("deadEnemies", deathEnemies);
        PlayerPrefs.SetInt("numRooms", numRooms);

    }
}
