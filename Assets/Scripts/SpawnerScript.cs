﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerScript : MonoBehaviour
{

    [SerializeField] GameObject[] enemies;
    

    float mapX = 100.0f;
    float mapY = 100.0f;

    private float minX;
    private float maxX;
    private float minY;
    private float maxY;

    private float width;
    private float height;

    // Start is called before the first frame update
    void Start()
    {

        RoomManager.OnRoomChange += SpawnEnemies;

         width = gameObject.GetComponent<Collider2D>().bounds.size.x;
         height = gameObject.GetComponent<Collider2D>().bounds.size.y;



        
        // Calculations assume map is position at the origin
        minX = width - mapX / 2.0f;
        maxX = mapX / 2.0f - width;
        minY = height - mapY / 2.0f;
        maxY = mapY / 2.0f - height;
        

        SpawnEnemies();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    
    void SpawnEnemies()
    {
        if (GameManager.Instance.numRooms%10 != 0)
        {

            int howMany = Random.Range(1, 4);

            for (int i = 0; i <= howMany; i++)
            {
                int whatEnemy = Random.Range(0, enemies.Length);

                //Vector3 where = new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY));
                Vector3 where = new Vector3(Random.Range(-8, 8), Random.Range(-4, 4));

                Instantiate(enemies[whatEnemy], transform.position + where, Quaternion.identity);
            }
        }
        
    }
}
