﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BulletMod", menuName = "Items/Passives/BulletMods", order = 1)]

public class BulletMod : Passive
{
    public GameObject bulletVariant;

    public BulletMod(GameObject bulletVariant)
    {
        this.bulletVariant = bulletVariant;
    }
}
