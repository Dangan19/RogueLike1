﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Weapon", menuName = "Items/Weapons", order = 1)]

public class Weapon : Items
{
    public float fireRate;
    public float bullets;
    public float spread;
    public float speed;
    public int damage;
    public int ammo;
    

    public Weapon(float fireRate, float bullets, float spread, float speed, int damage, int ammo)
    {
        this.fireRate = fireRate;
        this.bullets = bullets;
        this.spread = spread;
        this.speed = speed;
        this.damage = damage;
        this.ammo = ammo;
        
    }
}
