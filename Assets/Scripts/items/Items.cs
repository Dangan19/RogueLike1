﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Items : ScriptableObject
{
    public string name;
    public string description;
    public int tier;
    public int price;
    public Sprite image;
  
}
