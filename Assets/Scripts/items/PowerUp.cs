﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PowerUp", menuName = "Items/Pasives/PowerUps", order = 1)]

public class PowerUp : Passive
{
    [SerializeField] private boost statBoost;
    public double numberBoost;

    public enum boost
    {
        health,
        damage,
        speed,
        ammo,
        fireRate

    }

    public PowerUp(boost statBoost, double numberBoost)
    {
        this.statBoost = statBoost;
        this.numberBoost = numberBoost;
    }
}
