﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomManager : MonoBehaviour
{
    public delegate void ChangeRoom();
    public static event ChangeRoom OnRoomChange;

    public delegate void EndRoom();
    public static event EndRoom OnRoomEnd;

    private int howManyEnemies;
    [SerializeField] GameObject[] salas;
    [SerializeField] GameObject shopSala;
    private int currentSala = 0;
    public int salasCounter = 1;
    
    

    // Start is called before the first frame update
    void Start()
    {
        OnRoomChange += checkEnemies;
        OnRoomChange += NextRoom;
        OnRoomChange += PlusPoint;

        NextRoom();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        
         
        checkEnemies();

        if (howManyEnemies == 0)
        {
            FinishRoom();
        }

        
    }

    private void checkEnemies() {
        howManyEnemies = GameObject.FindGameObjectsWithTag("Enemy").Length;
        Debug.Log(howManyEnemies);

    }

    public void FinishRoom()
    {
        Debug.Log("ROOM CLEARED");

        if (OnRoomEnd != null)
        {
            OnRoomEnd();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            Debug.Log("Player change the room");
            
            if(OnRoomChange != null)
            {
                OnRoomChange();
            }
        }
    }

    void NextRoom()
    {
        
        GameObject salaAnterior = GameObject.FindGameObjectWithTag("Sala");
        Debug.Log(salaAnterior); 

        if (salaAnterior != null)
        {
            Destroy(salaAnterior);
        }

        currentSala = Random.Range(0, salas.Length);

        CleanScene();

        if(GameManager.Instance.numRooms% 10 == 0)
        {
            Instantiate(shopSala, new Vector3(0, 0, 0), Quaternion.identity);
        }
        else
        {
            Instantiate(salas[currentSala], new Vector3(0, 0, 0), Quaternion.identity);
        }

       
        GameManager.Instance.numRooms++;
        

        

    }

    void CleanScene()
    {

        GameObject[] bullets = GameObject.FindGameObjectsWithTag("bullet");
        foreach (GameObject bullet in bullets)
            GameObject.Destroy(bullet);

        GameObject[] ammoBoxs = GameObject.FindGameObjectsWithTag("AmmoBox");
        foreach (GameObject box in ammoBoxs)
            GameObject.Destroy(box);


        GameObject[] medikits = GameObject.FindGameObjectsWithTag("Medikit");
        foreach (GameObject kits in medikits)
            GameObject.Destroy(kits);
    }

    void PlusPoint()
    {
        GameManager.Instance.score += 50;
    }
}
