﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{

    [SerializeField] private Inventory inventory;

    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log(other);

        if (other.tag == "Medikit")
        {
            if (GameManager.Instance.playerHealth < GameManager.Instance.maxplayerHealth)
            {
                GameManager.Instance.playerHealth += other.GetComponent<Consumable>().quantity;
                Destroy(other.gameObject);
            }

        } else if (other.tag == "AmmoBox") {

            int ammo = other.GetComponent<Consumable>().quantity;

            if (ammo + GameManager.Instance.currentAmmo >= GameManager.Instance.maxAmmo)
            {
                GameManager.Instance.currentAmmo = GameManager.Instance.maxAmmo;
                
            }
            else
            {
                GameManager.Instance.currentAmmo += ammo;

            }

            Destroy(other.gameObject);

        }else if (other.tag == "SellGun")
        {
            SellItem si = other.GetComponent<SellItem>();

            if(si.price <= GameManager.Instance.score)
            {                
                inventory.PickUpGun(si.gun);
                Destroy(other.gameObject);
            }

        }

    }
}
