﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private float boost;
    [SerializeField] private Transform targetToFollow;
    [SerializeField] public int playerHealth;
    [SerializeField] private float spinVelocity;

    private Rigidbody2D rb;
    private float angle;
    private Vector3 dash_direction;
    private float normalSpeed;
    private bool dashAvailable = true;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        /*
        Vector3 mouse_pos_pixels = Input.mousePosition;
        Vector3 mouse_pos = Camera.main.ScreenToWorldPoint(mouse_pos_pixels);
        dash_direction = mouse_pos - transform.position;

        angle = Mathf.Atan2(dash_direction.y, dash_direction.x) * Mathf.Rad2Deg;
        */

      

        //dash_direction.Normalize();


        if (Input.GetKey("q")) 
        {

            transform.eulerAngles = new Vector3(0, 0, transform.eulerAngles.z + (spinVelocity * Time.deltaTime));

        }

        if (Input.GetKey("e"))
        {

            transform.eulerAngles = new Vector3(0, 0, transform.eulerAngles.z - (spinVelocity * Time.deltaTime)) ;

        }

        //dash
        if (Input.GetKey("left shift"))
        {
            if (dashAvailable) {

                StartCoroutine("DashColdonw");
                
            }

        }

        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //movement        
        float step = speed * Time.deltaTime;
        rb.MovePosition( Vector3.MoveTowards(transform.position, targetToFollow.position, step));
        

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Obstacle") {
            GameManager.Instance.playerDamaged(1000); 
        }
    }


    IEnumerator DashColdonw() {
        Debug.Log("DASH");        
        speed = boost;
        Physics2D.IgnoreLayerCollision(9, 8, true);
        gameObject.GetComponent<SpriteRenderer>().material.color = Color.blue;

        yield return new WaitForSeconds(0.5f);

        Physics2D.IgnoreLayerCollision(9, 8, false);
        gameObject.GetComponent<SpriteRenderer>().material.color = Color.white;

        speed = 3;
        dashAvailable = false;
        yield return new WaitForSeconds(3);
        dashAvailable = true;
    }

    

}
