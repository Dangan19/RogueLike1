﻿using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;

public class AimScript : MonoBehaviour
{ 

    private Vector3 firePoint;
    [SerializeField] private Transform targetToFollow;
    private Rigidbody2D rbBullet;
    [SerializeField] private Rigidbody2D bullet;
    [SerializeField] int currentAmmo;
    private Vector3 angle;
    private Quaternion targetRotation;
    public Gun gun = GunContainer.smallShotgun;
    protected bool readyToFire = true;
    float cntdnw;
    public Inventory inventory;
    


    // Start is called before the first frame update
    void Start()
    {
        cntdnw = gun.GetFireRate();
        
       

    }
    

    // Update is called once per frame
    void Update()
    {


        firePoint.x = transform.position.x;
        firePoint.y = transform.position.y;
        firePoint.z = 1;

        Vector2 shootVector = targetToFollow.position - firePoint;


       

        if (cntdnw > 0)
        {
            cntdnw -= Time.deltaTime;

        }
        else if (cntdnw < 0)
        {
            readyToFire = true;
        }

        if (Input.GetKey("space"))
        {
            /*
            rbBullet = Instantiate(bullet, firePoint, Quaternion.identity);
            shootVector.Normalize();
            rbBullet.velocity = shootVector * 5;
            */

            if (GameManager.Instance.currentAmmo > 0)
            {

                if (readyToFire)
                {
                    readyToFire = false;
                    SoundManagerScript.PlaySound("fire");




                    for (int i = 0; i < gun.GetBullets(); i++)
                    {
                        angle = Quaternion.Euler(0, 0, 180) * shootVector;
                        targetRotation = Quaternion.LookRotation(forward: Vector3.forward, upwards: angle);

                        rbBullet = Instantiate(bullet, firePoint, targetRotation);


                        ((BulletScript)rbBullet.GetComponent(typeof(BulletScript))).SetDamage(gun.GetDamage());

                        float size = gun.GetDamage() / 2;
                        if (size > 0.5f) size = 0.5f;
                        rbBullet.transform.localScale *= 1 + size;

                        Vector2 pdir = Vector2.Perpendicular(shootVector) * Random.Range(-gun.GetSpread(), gun.GetSpread());
                        rbBullet.velocity = (shootVector + pdir) * gun.GetSpeed();
                        GameManager.Instance.currentAmmo--;





                    }

                    cntdnw = gun.GetFireRate();

                }


            }
        }

    }

    public void SetGun(Gun g, int a) {
        gun = g;
        
       GameManager.Instance.maxAmmo = g.GetAmmo();
       GameManager.Instance.currentAmmo = a;
               
    }

   
}
