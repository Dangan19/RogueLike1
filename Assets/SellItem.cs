﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SellItem : MonoBehaviour
{
    public Gun gun;
    public Text textGun;
    public int price;

    // Start is called before the first frame update
    void Start()
    {
        gun = GunContainer.GetRandomGun();
        price = Random.Range(50, 100);
        textGun.text = gun.GetName() + "   " + price+ " $";
    }

}
