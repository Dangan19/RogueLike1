﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "WeaponData", menuName = "ScriptableObjects/WeaponData", order = 1)]
public class WeaponSc : ItemDataSC
{
    public int Damage;
    public int Ammo;
    public int MagCapacity;

    public string Name;
    public float FireRate;
    public int ShootsATM;

    public float ReloadTime;
}
