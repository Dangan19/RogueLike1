﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasController : MonoBehaviour
{
    private Canvas cl;

    // Start is called before the first frame update
    void Start()
    {
        cl = GetComponent<Canvas>();
        cl.worldCamera = Camera.main;
    }

}
